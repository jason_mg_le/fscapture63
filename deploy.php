<?php
if($_GET['UID'] != 'arcana_zxczxczx') die;
if(empty($_GET['branch'])) die;
$branch = $_GET['branch'];
$commands = array(
    'echo Abc@123456',
    'whoami',
    'git pull origin '.$branch,
    'git status',
    'php n98-magerun.phar c:f',
);
$output = '';
//var_dump($commands);exit();
foreach ($commands AS $command) {
    $tmp = shell_exec($command);
    $output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
    $output .= htmlentities(trim($tmp)) . "\n";
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
 .  ____  .    ____________________________
 |/      \|   |                            |
[| <span style="color: #FF0000;">&hearts;    &hearts;</span> |]  | Git Deployment Script v0.1 |
 |___==___|  /              &copy; oodavid 2012 |
              |____________________________|

    <?php echo $output; ?>
</pre>
</body>
</html>
